document.addEventListener("DOMContentLoaded", function() {
	(function ($) {

	  // phone mask
    $('input[name=phone]').mask('+380 00 000 00 00');

    // works-slider
    $('.owl-carousel').owlCarousel({
      // stagePadding: 50,
      autoWidth: true,
      loop: true,
      margin: 24,
      nav: true,
      dots: false,
      items: 1,
      navText: [
        '<img class="mob" src="../img/_src/arr_left_mob.svg" alt="arr-left"><img class="desc" src="../img/_src/arr_left.svg" alt="arr-left">',
        '<img class="mob" src="../img/_src/arr_right_mob.svg" alt="arr-right"><img class="desc" src="../img/_src/arr_right.svg" alt="arr-right">'
      ],
      responsive: {
        1200: {
          items: 2
        },
        1500: {
          items: 3
        }
      }
    });

    // image popup
    $('.works-slider .item').magnificPopup({
      delegate: 'a',
      type: 'image'
    });

    // reviews slider
    var mySwiper = new Swiper('.swiper-container', {
      spaceBetween: 20,
      slidesPerView: 4,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        991: {
          slidesPerView: 3,
        }
      }
    });

    // menu scroll
    $('.header .header__nav .header__nav_menu ul li a').click(function(e) {
      e.preventDefault();
      var id = $(this).attr('href'),
        top = $(id).offset().top;
      $('body,html').animate({scrollTop: top}, 800);
    });

    // hamburger
    $(".hamburger").click(function(){
      $(this).toggleClass("is-active");
      $(".mobile-header-menu").toggleClass('open');
      $(".header .header__nav").toggleClass('active');
    });
    $(".mobile-header-menu ul li a").click(function(e) {
      e.preventDefault();

      $(".hamburger").removeClass("is-active");
      $(".mobile-header-menu").removeClass('open');
      $(".header .header__nav").removeClass('active');

      var idM = $(this).attr('href'),
        topM = $(idM).offset().top;
      $('body,html').animate({scrollTop: topM}, 800);
    });

    // resize
    $(window).resize(function () {
      if ($(this).width() >= 769) {
        $(".hamburger").removeClass("is-active");
        $(".mobile-header-menu").removeClass('open');
        $(".header .header__nav").removeClass('active');
      }
    });

    // popup form
    $(".open-popup-form").magnificPopup({
      type:'inline'
    });

  })(jQuery);
});
